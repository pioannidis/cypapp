#!/usr/bin/env python3

import tkinter as tk
from PIL import ImageTk
import sqlite3
from datetime import datetime
#from numpy import random

bg_color = "#3d6466"

def uniq( in_list ):
  uniq_list = []             # initialize the final, unique list
  uniq_dict = set( in_list ) # generate a dict
  for x in uniq_dict:        # transfer the keys to uniq_list
    uniq_list.append( x )
  return uniq_list

def check_db():
  connection = sqlite3.connect( "sample_sqlite.db" )
  cursor = connection.cursor()
  
  # get the number of species in the database
  cursor.execute( "SELECT species_code FROM SEQUENCES;" )
  species = cursor.fetchall()
  species = uniq (species)

  # Since the above is a list of tuples,
  # convert it to a list of strings
  new_species = []
  for one_species in species:
    new_species.append ( one_species[0] )
  species = new_species

  status_message = "Database exists and contains " + str ( len (species) ) + " species (" + ", ".join (species) + ")"
  messages_lab.config (text = status_message)

  connection.close()

def search_db():
  pattern = textbox_ent.get()
  #print ( "Query pattern: " + pattern )
  
  connection = sqlite3.connect( "sample_sqlite.db" )
  cursor = connection.cursor()
  
  #fetch all tables
  cursor.execute( "SELECT name FROM sqlite_schema WHERE type = 'table';" )
  tables = cursor.fetchall()
  #print ( tables )
  
  # fetch gene names containing the 'query' string
  sql_query = "SELECT * FROM SEQUENCES WHERE protein_id like '%" + pattern + "%';"
  #print ( "SQL query: " + sql_query )
  cursor.execute( sql_query )
  matches_SEQ = cursor.fetchall() # matches from the SEQUENCES table
  
  # Count the number of matches
  num_matches = len (matches_SEQ)
  
  messages_lab.config (text = str( num_matches ) + " genes matching '" + pattern + "' were written to a tsv file\nwhich can be opened with a spreadsheet program (Calc, Excel, Numbers)" )

  #print ( "Total number of matches: " + str(num_matches) )
  
  # open the output file that will contain all the data from the search
  # the name of the file should be unique, but also informative!
  now = datetime.now()
  now = now.strftime("%Y-%m-%d__%H-%M-%S")
  filename = "CypApp_search." + pattern + "." + now + ".tsv"
  
  # open the file for output
  OUT = open ( filename, 'w')
  
  # also get the expression values for each of the above genes
  for match in matches_SEQ:
    protein_id =   match[0]
    cyp_name =     match[1]
    seq =          match[2]
    species_code = match[3]
    
    sql_query2 = "SELECT * FROM SEQUENCES INNER JOIN EXPRESSION_" + species_code.upper() + " ON SEQUENCES.protein_id = EXPRESSION_" + species_code.upper()  + ".protein_id WHERE SEQUENCES.protein_id = '" + protein_id + "';"
    cursor.execute( sql_query2 )
    matches_FULL = cursor.fetchall() # matches from the SEQUENCES AND the appropriate EXPRESSION table
                                     # this tuple contains data for ONE gene only!
    if len ( matches_FULL ) == 1:    # Just make sure you only got one match from the previous query
      # print all data for this gene in a tab-delimited format
      output_line = "\t".join( matches_FULL[0] )
      # For some reason, the sequence of the gene contains a \r character
      # which messes up with output. So, we need to remove it and because
      # it's in the middle of output_line, rstrip() doesn't work and I had
      # to use the replace() function.
      output_line = output_line.replace('\r', '')
      OUT.write ( output_line + "\n" )

  
  # close the file
  OUT.close()
  
  connection.close()

#SELECT * FROM SEQUENCES INNER JOIN EXPRESSION_HARMI ON SEQUENCES.protein_id = EXPRESSION_HARMI.protein_id WHERE SEQUENCES.protein_id like '%6AB%';


# initiallize app
root = tk.Tk()
root.title( "The CYP app" )
root.eval( "tk::PlaceWindow . center" )

# create the main frame
frame1 = tk.Frame( root, bg = bg_color )
frame1.grid( row = 0, column = 0, sticky = "nesw" )

# display the logo
logo_img = ImageTk.PhotoImage( file = "logo.png" )
logo_widget = tk.Label( frame1, image = logo_img, bg = bg_color )
logo_widget.image = logo_img
logo_widget.pack()


# welcome message (label)
welcome_lab = tk.Label(
  frame1,
  bg = bg_color,
  fg = "white",
  font = ( "TkHeadingFont", "20" ),
  text = "Welcome to the CYP app!",
)
welcome_lab.pack( pady=20, padx=20 )

# some basic instructions (label)
instructions_lab = tk.Label(
  frame1,
  bg = bg_color,
  fg = "white",
  text = 
  """
    Enter a search term in the text box below.
    Sample search terms:
    '6AB' will match 6AB1, 6AB10, and 6AB101 from ANY species
    '6A' will match 6AB, 6AE, 6AN etc genes from ANY species
    'Harmi_CYP6AB' will match 6AB genes from Helicoverpa armigera ONLY.
  """
)
instructions_lab.pack( pady=20, padx=20 )

# textbox for entering the search term (entry)
textbox_ent = tk.Entry(
  frame1,
  width = 50
)
textbox_ent.pack( pady=20, padx=20 )

# button for performing the search (button)
search_btn = tk.Button(
  frame1,
  text = "Search",
  width = 6,
  height = 1,
  font = ( "TkHeadingFont", 12 ),
  bg = bg_color,
  fg = "white",
  cursor = "hand2",
  activebackground = "#badee2",
  activeforeground = "black",
  command = search_db # the command (function) that will be executed upon clicking this button
)
search_btn.pack( pady=20, padx=20 )

# a label for displaying messages
messages_lab = tk.Label(
  frame1,
  bg = bg_color,
  fg = "white",
  text = "Useful messages will appear here!",
)
messages_lab.pack( pady=20, padx=20 )

# check that a database exists and show some details
check_db()

#load_frame1()

# run app
root.mainloop()
