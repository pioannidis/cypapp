# CypApp - A python-based gui for analyzing a custom CYP database

## Usage
Simply execute the file like so:

```
./CypApp.v1.py
```

The following Python libraries should be installed
(also see the import statements at the beginning of the script)
```
tkinter
PIL (ImageTk)
sqlite3
datetime
```

You should already have a SQLite database file, with the name "sample_sqlite.db" in the same directory as the script.
This database contains details for each P450, such as its name, species of origin, amino acid sequence, and expression
levels on conditions (e.g. developmental stages and tissues).

The current version of this database contains only two species, and it is updated as I am collecting CYPomes from more
and more insects. New versions of the database will be uploaded here.

## Support
You can send me a message here, or email me.
